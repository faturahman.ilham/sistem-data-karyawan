<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class Pegawai extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model("M_pegawai","pegawai");
    }

    public function index(){
    	$this->main->check_session();
		$url 				= $this->uri->segment(1);
		$module 			= "pegawai";
		$MenuID 			= "pegawai";
		$read_menu 			= $this->api->read_menu($MenuID);
		if($read_menu->view<=0): redirect('403_override'); endif;
		$btn_add = '';
		$btn_import = '';
		if($read_menu->insert>0): $btn_add = $this->main->button('add_new'); $btn_import = 'import'; endif;

		$data["title"] 		= "Master Data Pegawai";
		$data['url']		= $url;
		$data['module']		= $module;
		$data['content'] 	= 'backend/pegawai/list';
		$data['form']		= 'backend/pegawai/form';
		$data['filter']		= 'backend/pegawai/filter';
		$data['import']		= 'backend/pegawai/import';
		$data['btn_add']	= $btn_add;
		$data['btn_import']	= $btn_import;
		$this->load->view('backend/index',$data);
	}

	public function list(){
		$data 	= array();
		$list 	= $this->pegawai->get_datatables();
		$i 		= $this->input->post('start');

		$url 		= $this->input->post('page_url');
		$module 	= $this->input->post('page_module');
		$MenuID 	= "pegawai";
		$read_menu 	= $this->api->read_menu($MenuID);

		foreach ($list as $a):
			$no = $i++;
			$no += 1;
			$txt_status = $this->main->label_active($a->status);
			$tg_data = ' data-id="'.$a->id_pegawai.'" ';
			$tg_data .= ' data-status="'.$a->status.'" ';
			
			$btn_array = array();
			$btn_status = false;
			if($a->status == 0 AND $read_menu->update>0):
				$btn_status = true;
				array_push($btn_array, 'edit');
			endif;

			if($a->status == 0 AND $read_menu->delete>0):
				$btn_status = true;
				array_push($btn_array, 'delete');
			endif;

			if($btn_status):
				$btn = $this->main->button('action_list',$a->id_pegawai,$btn_array);
			else:
				$btn = '';
			endif;
			$rand = rand(1,8);
			$temp  = '<span class="dt-id" '.$tg_data.'>'.$no.'</span>';
			$image = '<img src="'.base_url('/assets/images/users/'.$rand.'.jpg').'" style="max-width:220px;object-fit:cover;background-position: center;"/>';

			$row = array();
			$row[] 	= $btn.$temp;
			$row[] 	= $image;
			$row[] 	= $a->nama;
			$row[] 	= $a->nama_jabatan;
			$row[] 	= $this->main->label_background($txt_status);
			$data[] = $row;
		endforeach;

		$response = array(
			"draw" 				=> $this->input->post('draw'),
			"recordsTotal" 		=> $this->pegawai->count_all(),
			"recordsFiltered" 	=> $this->pegawai->count_filtered(),
			"data" 				=> $data,
		);

		$this->main->echoJson($response);
	}

	public function save(){
		$url 				= $this->input->post('page_url');
		$module 			= $this->input->post('page_module');
		$MenuID 			= "pegawai";
		$read_menu 			= $this->api->read_menu($MenuID);

		$Crud 		= $this->input->post("crud");
		if($Crud == "insert"): $p3 = $read_menu->insert;
		elseif($Crud == "update"): $p3 = $read_menu->update;
		else: $p3 = 0;
		endif;

		$this->main->check_session('out',$Crud, $p3);
		$this->validation->pegawai();

		$ID 		= $this->input->post("ID");
		$Nama 		= $this->input->post("Nama");
		$Jabatan 	= $this->input->post("jabatan");

		$data = array(
			"nama"				=> $Nama,
			"id_jabatan"		=> $Jabatan,
		);
		if($Crud == "insert"):
			$message = "Insert Success";
			$this->main->general_save("mt_data_pegawai", $data);
		elseif($Crud == "update"):
			$message = "Update Success";
			$this->main->general_update("mt_data_pegawai", $data, array("id_pegawai" => $ID));
		endif;

		$response = array(
			"status"	=> true,
			"message"	=> $message,
		);
		$this->main->echoJson($response);
	}

	public function edit(){
		$url 				= $this->input->post('page_url');
		$module 			= $this->input->post('page_module');
		$MenuID 			= "pegawai";
		$read_menu 			= $this->api->read_menu($MenuID);
		$this->main->check_session('out', 'update', $read_menu->update);
		$ID 		= $this->input->post('ID');
		$list 		= $this->pegawai->get_by_id($ID);

		$response = array(
			"status"	=> true,
			"data"		=> $list,
			"message"	=> "Success",
		);

		$this->main->echoJson($response);
	}

	public function delete(){
		$url 				= $this->input->post('page_url');
		$module 			= $this->input->post('page_module');
		$MenuID 			= "pegawai";
		$read_menu 			= $this->api->read_menu($MenuID);

		$this->main->check_session('out', 'delete', $read_menu->delete);
		$ID = $this->input->post('ID');

		$this->db->where("id_pegawai", $ID);
		$this->db->delete("mt_data_pegawai");

		$response = array(
			"status"	=> true,
			"message"	=> "Success",
		);

		$this->main->echoJson($response);
	}

	public function export(){
		$this->pegawai->export('template');
	}

	public function save_import(){
		$p1 = $this->input->post('p1');
		if($p1 == 'save'):
			$this->proccess_save_import();
		else:
			$this->upload_import();
		endif;
	}

	private function upload_import(){

		// $this->validation->user_company_import();
	
		$folder = $this->main->create_folder_general('import_pegawai');

		$nmfile                     = "test_".date("ymdHis");
	    $config['upload_path']      = './'.$folder; //path folder
	    $config['file_name']      	= $nmfile;
		$config['allowed_types']  	= 'csv';
		$config['max_size']       	= 10000;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('file')):
			$output = array(
				"status" 	=> false,
				"message"	=> "Data Kosong",
			);
		else:
			$media 			= $this->upload->data();
			$inputFileName 	= $folder.$media['file_name'];
			try {
				$inputFileType  = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
				$objReader      = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
				$objPHPExcel  	= $objReader->load($inputFileName);
			}
			catch(Exception $e) {
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			$sheet         = $objPHPExcel->getSheet(0);
			$highestRow    = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			$status    = true;
			$message   = "success";

			$arrData   = array();
			$arrHeader = array(); 
			$rowData   = $sheet->rangeToArray('A' . 1 . ':' . $highestColumn . 1,NULL,TRUE,FALSE);

			if($rowData): $arrHeader = $rowData; endif;
			$total_data = 0;
			$arrCode 	= array();
			for ($row = 2; $row <= $highestRow; $row++){
				$total_data += 1;
				$rowData 		= $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL,TRUE,FALSE);

				// print_r(count($rowData[0])); exit();

				if(count($rowData[0]) == 3):
					$Message 		= '';


					$status_product = true;
					$status_data 	= "insert";
					$No 			= $this->main->checkInputData($rowData[0][0]);
					$nama_pegawai	= $this->main->checkInputData($rowData[0][1]);
					$nama_jabatan	= $this->main->checkInputData($rowData[0][2]);

					$ck_pegawai = $this->db->count_all("mt_data_pegawai where nama = '$nama_pegawai'");
					if(!$nama_pegawai):
						$status_product = false;
						$Message .= "- "."Nama Pegawai Tidak Boleh Kosong"."<br>";
					elseif($ck_pegawai>0):
						$status_product = false;
						$Message .= "- "."Nama Pegawai Sudah Ada"."<br>";
					endif;
					array_push($arrCode,$nama_pegawai);

					$ck_jabatan = $this->db->count_all("mt_data_jabatan where nama_jabatan = '$nama_jabatan'");
					if(!$nama_jabatan):
						$status_product = false;
						$Message .= "- "."Nama Jabatan Tidak Boleh Kosong"."<br>";
					elseif($ck_jabatan>0):
						$status_product = false;
						$Message .= "- "."Nama Jabatan Sudah Ada"."<br>";
					endif;

			        $h = array(
				    	"status"		=> $status_product,
				    	"status_data"	=> $status_data,
				    	"nama_pegawai"	=> $nama_pegawai,
				    	"nama_jabatan"	=> $nama_jabatan,
				    	"Message" 		=> $Message,
				    );

				    array_push($arrData, $h);


				else:
					$status  = false;
					$message = "Column Tidak Sesuai";
				endif;
			}

			if($total_data<=0):
				$status = false;
				$message = "Data Tidak Ditemukan";
			endif;

			$output = array(
				"status" 	 	=> $status,
				"message"		=> $message,
				"data"		 	=> $arrData,
				"header"	 	=> $arrHeader,
				"inputFileName" => $inputFileName,
			);
		endif;

		$this->main->echoJson($output);
	}

	private function proccess_save_import(){
		$ID 			= $this->input->post('ID');
		$inputFileName 	= $this->input->post('inputFileName');

		if(is_file("./".$inputFileName)):
			$status  = true;
			$message = "Import Data Success";

			try {
				$inputFileType  = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
				$objReader      = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
				$objPHPExcel  	= $objReader->load($inputFileName);
			}
			catch(Exception $e) {
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			$sheet         = $objPHPExcel->getSheet(0);
			$highestRow    = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			$arrData   = array();
			$arrHeader = array(); 
			$rowData   = $sheet->rangeToArray('A' . 1 . ':' . $highestColumn . 1,NULL,TRUE,FALSE);
			if($rowData): $arrHeader = $rowData; endif;
			$total_data = 0;
			$arrCode 	= array();
			for ($row = 2; $row <= $highestRow; $row++){
				$total_data += 1;
				$rowData 		= $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL,TRUE,FALSE);

				if(count($rowData[0]) == 3):
					$status_product = true;
					$status_data 	= "insert";
					$nama_pegawai	= $this->main->checkInputData($rowData[0][1]);
					$nama_jabatan	= $this->main->checkInputData($rowData[0][2]);

					$ck_pegawai = $this->db->count_all("mt_data_pegawai where nama = '$nama_pegawai'");
					if(!$nama_pegawai):
						$status_product = false;
					elseif($ck_pegawai>0):
						$status_product = false;
					endif;
					array_push($arrCode,$nama_pegawai);

					$ck_jabatan = $this->db->count_all("mt_data_jabatan where nama_jabatan = '$nama_jabatan'");
					if(!$nama_jabatan):
						$status_product = false;
					elseif($ck_jabatan>0):
						$status_product = false;
					endif;


			        if($status_product):
			        	$data_import_jabatan = array(
			        		'nama_jabatan'	=> $nama_jabatan,
			        	);
						$ID = $this->main->general_save("mt_data_jabatan", $data_import_jabatan);

			        	$data_import_pegawai = array(
			        		'nama'			=> $nama_pegawai,
			        		'id_jabatan'	=> $ID,
			        	);
			        	$this->main->general_save("mt_data_pegawai", $data_import_pegawai);
			        endif;
				endif;
			}

			$output = array(
				"status" 	 	=> $status,
				"message"		=> $message,
			);
		else:
			$status  = false;
			$message = "File Tidak Ditemukan";
		endif;

		$output = array(
			"status" 	=> $status,
			"message"	=> $message,
		);

		$this->main->echoJson($output);
	}
}