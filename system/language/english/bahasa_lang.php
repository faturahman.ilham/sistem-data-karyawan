
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
#general
$lang['lb_app_name'] 			= "Template Default";
$lang['lb_login']				= "Login";
$lang['lb_username']			= "Username";
$lang['lb_password']			= "Password";
$lang['lb_remember_me']			= "Remember Me";
$lang['lb_forgot_pwd']			= "Forgot Pwd?";
$lang['lb_dont_have_account']	= "Don't have an account?";
$lang['lb_register'] 			= "Register";
$lang['lb_success']				= "Success";
$lang['lb_name']				= "Name";
$lang['lb_name_first'] 			= "First Name";
$lang['lb_name_last']			= "Last Name";
$lang['lb_name_full']			= "Full Name";
$lang['lb_name_nickname'] 		= "Nickname";
$lang['lb_departement']			= "Departement";
$lang['lb_email']				= "Email";
$lang['lb_cancel']				= "Cancel";
$lang['lb_save']				= "Save";
$lang['lb_save_new']			= "Save & New";
$lang['lb_no']					= "No";
$lang['lb_add_new']				= "Add New";
$lang['lb_add_data']			= "Add Data";
$lang['lb_edit_data']			= "Edit Data";
$lang['lb_list_data']			= "List Data";
$lang['lb_success_insert'] 		= "Insert Success";
$lang['lb_success_update'] 		= "Update Success";
$lang['lb_success_active']		= "Active Data Success";
$lang['lb_success_nonactive']	= "Nonactive Data Success";
$lang['lb_remark']				= "Remark";
$lang['lb_close']				= "Close";
$lang['lb_access_insert'] 		= "You don't have access Add Data";
$lang['lb_access_update'] 		= "You don't have access Update Data";
$lang['lb_access_delete'] 		= "You don't have access Delete/Nonactive/Active Data";
$lang['lb_access_approve'] 		= "You don't have access Approve Data";
$lang['lb_access_data'] 		= "You don't have access Data";
$lang['lb_for'] 				= "For";
$lang['lb_search']				= "Search";
$lang['lb_export']				= "Export";
$lang['lb_import']				= "Import";
$lang['lb_iso_country']			= "Iso Country";
$lang['lb_note']				= "Note";
$lang['lb_result']				= "Result";
$lang['lb_success_import']		= "Import Success";
$lang['lb_code'] 				= "Code";
$lang['lb_day'] 				= "Day";
$lang['lb_from'] 				= "From";
$lang['lb_to'] 					= "To";
$lang['lb_address'] 			= "Address";
$lang['lb_latitude'] 			= "Latitude";
$lang['lb_longitude'] 			= "Longitude";
$lang['lb_radius'] 				= "Radius";
$lang['lb_automatic'] 			= "Automatic";
$lang['lb_company_profile']		= "Company Profile";
$lang['lb_profile']				= "Profile";
$lang['lb_meters']				= "Meters";
$lang['lb_data']				= "Data";
$lang['lb_general']				= "General";
$lang['lb_website']				= "Website";

// menu
$lang['lb_menu'] 				= "Menu";
$lang['lb_root']				= "Root";
$lang['lb_url']					= "Url";
$lang['lb_type']				= "Type";
$lang['lb_level']				= "Level";
$lang['lb_backend']				= "Backend";
$lang['lb_frontend']			= "Frontend";
$lang['lb_parent']				= "Parent";
$lang['lb_incomplate_form']		= "Incomplate Form";

// validation
$lang['lb_code_null']			= "Code can't be null";
$lang['lb_name_null']			= "Name can't be null";
$lang['lb_root_null']			= "Root can't be null";
$lang['lb_url_null']			= "Url can't be null";
$lang['lb_parent_null']			= "Parent can't be null";
$lang['lb_delete_data']			= "Delete Data";
$lang['lb_parent_not_found'] 	= "Parent not found";
$lang['lb_status']				= "Status";
$lang['lb_email_null'] 			= "Email can't be null";
$lang['lb_email_validate']		= "Format email invalid";
$lang['lb_email_already']		= "Email already registered";
$lang['lb_code_already']		= "Code already registered";
$lang['lb_code_max']			= "Code must contain at least 3 characters";
$lang['lb_role_null']			= "Role can't be null";	
$lang['lb_phone_null']			= "Phone can't be null";	
$lang['lb_iso_null']			= "Iso Country can't be null";	
$lang['lb_password_null']		= "Password can't be null";
$lang['lb_password_max'] 		= "Password must contain at least 8 characters";
$lang['lb_password_numb'] 		= "Password must contain at least 1 number";
$lang['lb_password_upper'] 		= "Password must contain at least 1 Capital letter";
$lang['lb_password_lower'] 		= "Password must contain at least 1 Lowercase letter";
$lang['lb_work_date_null']		= "Start Work Date can't be null";
$lang['lb_username_null']		= "Username can't be null";
$lang['lb_username_max'] 		= "Username must contain at least 6 characters";
$lang['lb_imei_null']			= "Imei can't be null";
$lang['lb_company_null']		= "Company can't be null";
$lang['lb_excell_empty']		= "Please Upload Excel File";
$lang['lb_file_not_found'] 		= 'File not found';
$lang['lb_data_not_found']		= "Data not found";
$lang['lb_column_not_match']	= "Column not match";
$lang['lb_work_pattern_null'] 	= "Work Pattern can't be null";
$lang['lb_start_join_null']		= "Start Join Date can't be null";
$lang['lb_location_name_null'] 	= "Location Name can't be null";
$lang['lb_address_null'] 		= "Address can't be null";
$lang['lb_latitude_null']  		= "Latitude can't be null";
$lang['lb_longitude_null'] 		= "Longitude can't be null";
$lang['lb_radius_null'] 		= "Radius can't be null";
$lang['lb_departement_null'] 	= "Departement can't be null";


// users
$lang['lb_users'] 	= "Users";
$lang['lb_role']	= "Role";
$lang['lb_active']	= "Active";
$lang['lb_nonactive'] = "Nonactive";
$lang['lb_company'] = "Company";
$lang['lb_gender']	= "Gender";
$lang['lb_male']	= "Male";
$lang['lb_female']	= "Female";
$lang['lb_photo']	= "Photo";
$lang['lb_photo_profile']	= "Photo Profile";
$lang['lb_start_work_date'] = "Start Work Date";
$lang['lb_phone_number']	= "Phone Number";
$lang['lb_imei']			= "Imei";
$lang['lb_mandatory_imei'] 	= "Mandatory Imei";
$lang['lb_start_join'] 		= "Start Join Date";
$lang['lb_logo'] 			= "Logo Company";
$lang['lb_location'] 		= "Location";
$lang['lb_location_name'] 	= "Location Name";

// icon
$lang['lb_icon'] 	= "Icon";

// Leave
$lang['lb_leave']	= "Leave";

// work pattern
$lang['lb_work_pattern'] 	= "Work Pattern";
$lang['lb_day_cycle'] 		= "Day(s) Cycle";
$lang['lb_work_hours']		= "Work Hours";
$lang['lb_work_day']		= "Working Day";
$lang['lb_work_number']		= "Number of days cycle";
$lang['lb_work_apply'] 		= "Apply Tardiness Tolerance";
$lang['lb_tardiness_toleran'] = "Tardiness Tolerance";
$lang['lb_toleran'] 		= "Tolerance";
$lang['lb_minutes'] 		= "Minutes";