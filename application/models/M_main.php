<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_main extends CI_Model {
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function echoJson($data){
        header('Content-Type: application/json');
        echo json_encode($data,JSON_PRETTY_PRINT);
    }

        #button
        public function button($p1,$p2="",$p3="",$p4="",$p5=""){
            $btn = '';
            if($p1 == "action"):
                $btn = '<div class="row">
                            <div class="col-md-12">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-danger" onclick="open_form('."'close'".')"><i class="fa fa-close"></i>Cancel</button>
                                    <button type="button" class="btn btn-primary btn-save v-Save" onclick="save('."'save'".')"><i class="fa fa-save"></i>Save</button>
                                    <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split v-Save" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu animated flipInY" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 40px; left: 0px; will-change: transform;">
                                        <a class="dropdown-item" href="javascript:;" onclick="save('."'save_new'".')">Save & New</a>
                                    </div>
                                </div>
                            </div>
                        </div>';
            elseif($p1 == 'action2'):
                $btn = '<div class="row">
                            <div class="col-md-12">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-primary btn-save" onclick="save('."'save'".')"><i class="fa fa-save"></i>Save</button>
                                </div>
                            </div>
                        </div>';
            elseif($p1 == "action_import"):
                $btn = '<div class="row">
                            <div class="col-md-12">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-danger" onclick="open_form('."'close'".')"><i class="fa fa-close"></i>Cancel</button>
                                    <button type="button" class="btn btn-primary btn-save" onclick="import_data('."'import'".')"><i class="fa fa-save"></i>Import</button>
                                </div>
                            </div>
                        </div>';
            elseif($p1 == "action_import2"):
                $btn = '<div class="row">
                            <div class="col-md-12">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-danger" onclick="open_form('."'close'".')"><i class="fa fa-close"></i>Cancel</button>
                                    <button type="button" class="btn btn-primary btn-save" onclick="import_data('."'save'".')"><i class="fa fa-save"></i>Save</button>
                                </div>
                            </div>
                        </div>';
            elseif($p1 == "add_new"):
                $btn = '<button type="button" onclick="open_form()" class="btn btn-add btn-add d-lg-block m-l-15"><i class="fa fa-plus-circle"></i>Add New</button>';
            elseif($p1 == "close"):
                $btn = '<button type="button" class="btn btn-danger btn-close" data-dismiss="modal"><i class="fa fa-close"></i>Close</button>';
            elseif($p1 == "action_list"):
                $edit = ''; $nonactive = ''; $active = ''; $approve = ''; $rejected = ''; $view = ''; $reset_device = '';
                if(in_array('edit',$p3)):
                    $edit = '<a class="dropdown-item action-edit" onclick="action_edit()" href="javascript:;">Edit Data</a>';
                endif;
                if(in_array('nonactive', $p3)):
                    $nonactive = '<a class="dropdown-item action-delete" onclick="action_delete()" href="javascript:;">Nonactive</a>';
                endif;
                if(in_array('active', $p3)):
                    $active = '<a class="dropdown-item action-delete" onclick="action_delete()" href="javascript:;">Active</a>';
                endif;
                if(in_array('approve',$p3)):
                    $approve = '<a class="dropdown-item action-approve" onclick="action_approve()" href="javascript:;">Approve</a>';
                endif;
    
                if(in_array('rejected', $p3)):
                    $rejected = '<a class="dropdown-item action-rejected" onclick="action_rejected()" href="javascript:;">Reject</a>';
                endif;
    
                if(in_array('delete', $p3)):
                    $active = '<a class="dropdown-item action-delete" onclick="action_delete()" href="javascript:;">Delete</a>';
                endif;
    
                if(in_array('view', $p3)):
                    $view = '<a class="dropdown-item action-view" onclick="action_view()" href="javascript:;">View</a>';
                endif;
    
                if(in_array('reset_device', $p3)):
                    $reset_device = '<a class="dropdown-item action-view" onclick="action_reset_device()" href="javascript:;">Reset Device</a>';
                endif;
    
                $btn = '<div class="btn-group" role="group">
                    <div data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></div>
                    <div class="dropdown-menu action_list" aria-labelledby="btnGroupVerticalDrop1" x-placement="top-start">
                        '.$view.$edit.$nonactive.$active.$approve.$rejected.$reset_device.'
                    </div>
                </div>';
    
            elseif($p1 == "filter"):
                $search = ''; $import = ''; $export = '';
                $pdf = ''; $excel = '';
                if(in_array('search', $p2)):
                    $search = '<button type="button" class="btn btn-primary" onclick="filter_table()">
                    <i class="fa fa-search"></i> Search</button>';
                endif;
                if(in_array('export', $p2)):
                    $export = '<button type="button" class="btn btn-info" onclick="export_data()">
                    <i class="fa fa-files-o"></i>Export</button>';
                endif;
                if(in_array('import', $p2)):
                    $import = '<button type="button" class="btn btn-info" onclick="open_form('."'import'".')">
                    <i class="fa fa-file"></i>Import</button>';
                endif;
                if(in_array('pdf',$p2)):
                    $pdf = '<button type="button" class="btn btn-info dropdown-toggle btn-pdf" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius:0px">
                            Export PDF
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu animated flipInY" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 40px; left: 0px; will-change: transform;">
                            <a class="dropdown-item" href="javascript:;" onclick="export_data('."'pdf'".')">Potrait</a>
                            <a class="dropdown-item" href="javascript:;" onclick="export_data('."'pdf','lanscape'".')">Landscape</a>
                        </div>';
                endif;
                if(in_array('excel',$p2)):
                    $excel = '<button type="button" class="btn btn-info btn-excell" onclick="export_data('."'excel'".')">
                    <i class="fa fa-files-o"></i> Export Excel</button>';
                endif;
                $btn = '<div class="btn-group" role="group" aria-label="Basic example">
                        '.$export.$import.$pdf.$excel.$search.'
                    </div>';
    
            endif;
    
            return $btn;
    
        }

        public function check_session($p1="",$p2="",$p3=""){
            if($p1 == "out"):
                $p1 = true;
                $message = '';
                $arrP2 = array('insert','update','delete','approve','view');
                if($p2 == "insert" && !$p3):
                    $p1      = false;
                    $message = $this->lang->line('lb_access_insert');
                elseif($p2 == "update" && !$p3):
                    $p1      = false;
                    $message = $this->lang->line('lb_access_update');
                elseif($p2 == "delete" && !$p3):
                    $p1      = false;
                    $message = $this->lang->line('lb_access_delete');
                elseif($p2 == "approve" && !$p3):
                    $p1      = false;
                    $message = $this->lang->line('lb_access_approve');
                elseif($p2 == "view" && !$p3):
                    $p1      = false;
                    $message = $this->lang->line('lb_access_view');
                elseif($p2 && !in_array($p2,$arrP2)):
                    $p1      = false;
                    $message = $this->lang->line('lb_access_data');
                endif;
            endif;
        }

        public function label_active($p1,$p2=""){
            $label = '';
            if($p1 == 1):
                $label = "Terkontrak";
                elseif($p1 == 0):
                $label = "Tersedia";
            endif;
            return $label;
        }

        public function label_background($p1){
            $arrSuccess  = array('Tersedia');
            $arrDanger = array('Terkontrak');
            $p2 = '';
            if(in_array($p1,$arrSuccess)):
                $p2 = ' txt-success ';
                elseif(in_array($p1, $arrDanger)):
                $p2 = ' txt-danger ';
            endif;
            return '<span class="'.$p2.'">'.$p1.'</span>';
    
        }

        public function label_kirim($p1,$p2=""){
            $label = '';
            if($p1 == 1):
                $label = "Terkirim";
                elseif($p1 == 0):
                $label = "Gagal";
            endif;
            return $label;
        }

        public function label_background_kirim($p1){
            $arrSuccess  = array('Terkirim');
            $arrDanger = array('Gagal');
            $p2 = '';
            if(in_array($p1,$arrSuccess)):
                $p2 = ' txt-success ';
                elseif(in_array($p1, $arrDanger)):
                $p2 = ' txt-danger ';
            endif;
            return '<span class="'.$p2.'">'.$p1.'</span>';
    
        }
    
        #general_save
        public function general_save($table,$data){
            $this->db->set("UserAdd",$this->session->Name);
            $this->db->set("DateAdd",date("Y-m-d H:i:s"));
            $this->db->insert($table,$data);
            return $this->db->insert_id();
        }
    
        #general update
        public function general_update($table,$data,$where){
            $this->db->set("UserCh",$this->session->Name);
            $this->db->set("DateCh",date("Y-m-d H:i:s"));
            $this->db->update($table, $data, $where);
            return $this->db->affected_rows();
        }

        public function tanggal_indo($tanggal) {
            $bulan = array (1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $split = explode('-', $tanggal);
            return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
        }

        public function create_folder_temp(){
            $folder = 'file/temp'.date("Y-m-d").'/';
            if (!is_dir($folder)) {
                mkdir('./'.$folder, 0777, TRUE);
            }
        }
    
        public function create_folder_general($page){
            $this->create_folder_temp();
            $temp   = 'file/temp'.date("Y-m-d").'/';
            $folder = $temp.$page."/";
            if (!is_dir($folder)) {
                mkdir('./'.$folder, 0777, TRUE);
            }
    
            $files = glob('./'.$folder.'*.*');
            foreach($files as $file){
                if(is_file($file))
                    unlink($file);
            }
            return $folder;
        }

        public function checkInputData($input){
            $data = '';
            if($input):
                $data = $input;
            endif;
            return $data;
        }

        public function data_send_mail(){
            $this->db->select("
                mt_mail.id_email,
                mt_mail.to,
                mt_mail.subject,
                mt_mail.isi,
                mt_mail.date,
                mt_mail.status,
            ");
            $this->db->from("mt_mail");
            
            $query = $this->db->get();
    
            return $query->result();
        }

        public function send_email($page,$code,$cek_page = "",$p1=""){
            if($page == "send"):
                $a                  = $this->data_send_mail();
                foreach($a as $k => $v){
                    $to                = $v->to;
                    $isi               = $v->isi;
                    $subject           = $v->subject;
                    $page_email        = "email/index";
                    $data["message"]   = $v->isi;
                    $data["page"]      = "email/send";
                }
            endif;
    
            $data["to"]      = $to;
            $data["isi"]     = $isi;
            $data["subject"] = $subject;

            if($cek_page == "ya"):
                $this->load->view($page_email,$data);
            else:
                $data_email = $this->db->query("SELECT * FROM ut_smtp");
                if ($data_email->num_rows() > 0):
                    $hasil      = $data_email->row(1);
                    $protocol   = $hasil->protocol;
                    $smtp_host  = $hasil->smtp_host;
                    $smtp_port  = $hasil->smtp_port;
                    $smtp_user  = $hasil->smtp_user;
                    $smtp_pass  = $hasil->smtp_pass;
                endif;
                $config = Array(
                    'useragent'     => "Codeigniter",
                    'protocol'      => $protocol,
                    'smtp_host'     => $smtp_host,
                    'smtp_port'     => $smtp_port,
                    'smtp_user'     => $smtp_user,
                    'smtp_pass'     => $smtp_pass,
                    'mailtype'      => 'html',
                    'charset'       => 'iso-8859-1',
                    'wordwrap'      => TRUE,
                    'newline'       => "\r\n",
                );
                $this->load->library('email');  
                $this->email->initialize($config);
                $this->email->set_newline("\r\n");
                $this->email->set_mailtype("html");
                $this->email->subject("Sistem Data Karyawan"." - ".$subject);
                $this->email->from($smtp_user,'Sistem Data Karyawan');
                $this->email->to($to);
                $this->email->message($this->load->view($page_email, $data, TRUE));
                $send = $this->email->send();
                if($send):
                    
                else:
                    if($p1 == "yes"):
                        $error = $this->email->print_debugger();
                        print_r($error);
                    endif;
                endif;
            endif;
        }
}