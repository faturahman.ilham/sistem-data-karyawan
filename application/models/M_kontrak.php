<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_kontrak extends CI_Model {
	var $table = 'mt_data_kontrak';
	var $column = array("mt_data_pegawai.nama"); //set column field database for order and search
	var $order = array('mt_data_kontrak.id_kontrak' => 'desc'); // default order 

	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    private function _get_datatables_query()
	{
		$table 			= $this->table;
		$searchx 		= $this->input->post('Searchx');

		$this->db->select("
			$table.id_kontrak,
			$table.awal_kontrak,
			$table.akhir_kontrak,
			$table.status,

			mt_data_pegawai.nama as nama_pegawai
		");

		$this->db->from($this->table);
        $this->db->join("mt_data_pegawai", "mt_data_pegawai.id_pegawai = $table.id_pegawai");

		$i = 0;
	
		foreach ($this->column as $item) // loop column 
		{
			if($searchx) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND. 
					$this->db->like($item, $searchx);
				}
				else
				{
					$this->db->or_like($item, $searchx);
				}

				if(count($this->column) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$column[$i] = $item; // set column array variable to order processing
			$i++;
		}
		
		if($this->input->post('order')) // here order processing
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($this->input->post('length') != -1)
		$this->db->limit($this->input->post('length'), $this->input->post('start'));
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{

		$searchx 		= $this->input->post('Searchx');

		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($p1){

        $table 			= $this->table;

		$this->db->select("
            $table.id_kontrak,
            $table.nama_kontrak,
            $table.awal_kontrak,
            $table.akhir_kontrak,
            $table.Status,
		");

		$this->db->from($this->table);
        $this->db->join("mt_data_jabatan jab", "jab.id_jabatan = $table.id_jabatan");
        $this->db->join("mt_data_pegawai peg", "peg.id_pegawai = $table.id_pegawai");
		$this->db->where("mt_data_kontrak.id_kontrak", $p1);

		$query = $this->db->get();

		return $query->row();
	}

}