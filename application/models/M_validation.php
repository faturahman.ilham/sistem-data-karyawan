<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_validation extends CI_Model {
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function jabatan(){
    	$data                   = array();
        $data['error_string']   = array();
        $data['inputerror']     = array();
        $data['input_type']     = array();
        $data['status']         = TRUE;
        $message                = "Incomplete form";

		$Nama 		= $this->input->post("Nama");
        $ck_nama    = $this->api->get_one_row("mt_data_jabatan","nama_jabatan",array("nama_jabatan" => $Nama));

        if($Nama == ''):
            $data['inputerror'][]   = 'Nama';
            $data['input_type'][]   = '';
            $data['error_string'][] = "Nama jabatan tidak boleh kosong";
            $data['status'] = FALSE;
        elseif($ck_nama):
            $data['inputerror'][]   = 'Nama';
            $data['input_type'][]   = '';
            $data['error_string'][] = "Nama jabatan sudah ada";
            $data['status'] = FALSE;
        endif;

    	if($data['status'] === FALSE):
            $data['message'] = $message;
            echo json_encode($data);
            exit();
        endif;

    }
    public function pegawai(){
    	$data                   = array();
        $data['error_string']   = array();
        $data['inputerror']     = array();
        $data['input_type']     = array();
        $data['status']         = TRUE;
        $message                = "Incomplete form";

		$Nama 		= $this->input->post("Nama");
        $ck_nama    = $this->api->get_one_row("mt_data_pegawai","nama",array("nama" => $Nama));

        if($Nama == ''):
            $data['inputerror'][]   = 'Nama';
            $data['input_type'][]   = '';
            $data['error_string'][] = "Nama pegawai tidak boleh kosong";
            $data['status'] = FALSE;
        elseif($ck_nama):
            $data['inputerror'][]   = 'Nama';
            $data['input_type'][]   = '';
            $data['error_string'][] = "Nama pegawai sudah ada";
            $data['status'] = FALSE;
        endif;

    	if($data['status'] === FALSE):
            $data['message'] = $message;
            echo json_encode($data);
            exit();
        endif;

    }

    public function mail(){
    	$data                   = array();
        $data['error_string']   = array();
        $data['inputerror']     = array();
        $data['input_type']     = array();
        $data['status']         = TRUE;
        $message                = "Incomplete form";

        $to 		= $this->input->post("to");
		$subject 	= $this->input->post("subject");

        if($to == ''):
            $data['inputerror'][]   = 'to';
            $data['input_type'][]   = '';
            $data['error_string'][] = "Email tujuan tidak boleh kosong";
            $data['status'] = FALSE;
        endif;

        if($subject == ''):
            $data['inputerror'][]   = 'subject';
            $data['input_type'][]   = '';
            $data['error_string'][] = "Subject tujuan tidak boleh kosong";
            $data['status'] = FALSE;
        endif;

    	if($data['status'] === FALSE):
            $data['message'] = $message;
            echo json_encode($data);
            exit();
        endif;

    }
}