<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->dbforge();
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
    }

    public function get_mail(){
        $data = $this->api->get_mail();
        $this->main->echoJson($data);
    }

    public function test(){
        
        $this->main->send_email("send",1,"ya");
        // $this->main->echoJson($data);
    }

    public function save_mail(){
		$url 				= $this->input->post('page_url');
		$module 			= $this->input->post('page_module');
		$MenuID 			= 1;
		$read_menu 			= $this->api->read_menu($MenuID);

		$Crud 		= $this->input->post("crud");
		if($Crud == "insert"): $p3 = $read_menu->insert;
		elseif($Crud == "update"): $p3 = $read_menu->update;
		else: $p3 = 0;
		endif;

		$this->main->check_session('out',$Crud, $p3);
		// $this->validation->users();

		$protocol 			= $this->input->post("protocol");
		$smtp_host 			= $this->input->post("smtp_host");
		$smtp_port 		    = $this->input->post("smtp_port");
		$smtp_user 			= $this->input->post("smtp_user");
		$smtp_pass 			= $this->input->post("smtp_pass");

		$data = array(
			"protocol"		=> $protocol,
			"smtp_host"		=> $smtp_host,
			"smtp_port" 	=> $smtp_port,
			"smtp_user"	    => $smtp_user,
			"smtp_pass"	    => $smtp_pass,
		);

        $message = "Update data success";
        $this->main->general_update("ut_smtp", $data, array("id" => 1));

		$response = array(
			"status"	=> true,
			"message"	=> $message,
		);
		$this->main->echoJson($response);
	}
}