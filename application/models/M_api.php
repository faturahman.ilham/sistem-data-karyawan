<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_api extends CI_Model {
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function read_menu(){
        
        $view       = 1;
        $insert     = 1;
        $update     = 1;
        $delete     = 1;

        $data = array(
            'view'      => $view,
            'insert'    => $insert,
            'update'    => $update,
            'delete'    => $delete,
        );
        $data = json_encode($data);
        $data = json_decode($data);

        return $data;

        $data = array(
            'view'      => 0,
            'insert'    => 0,
            'update'    => 0,
            'delete'    => 0,
        );
        $data = json_encode($data);
        $data = json_decode($data);
        return $data;
    }

    public function get_one_row($table,$column,$where){
        $this->db->select($column);
        $this->db->where($where);
        $query = $this->db->get($table);

        return $query->row();
    }

    public function jabatan(){
        $this->db->select("
            id_jabatan as ID,
            nama_jabatan,
            status,
        ");
        $this->db->from("mt_data_jabatan");
        $this->db->where("status", 0);
        
        $query = $this->db->get();

        return $query->result();
    }

    public function pegawai(){
        $this->db->select("
            mt_data_pegawai.id_pegawai as ID,
            mt_data_pegawai.id_jabatan,
            mt_data_pegawai.nama,
            mt_data_pegawai.status,

            mt_data_jabatan.nama_jabatan,
        ");
        $this->db->from("mt_data_pegawai");
        $this->db->join("mt_data_jabatan", "mt_data_pegawai.id_jabatan = mt_data_jabatan.id_jabatan");
        $this->db->where("mt_data_pegawai.status", 0);
        
        $query = $this->db->get();

        return $query->result();
    }

    public function get_pegawai(){
        $this->db->select("
            mt_data_pegawai.id_pegawai as ID,
            mt_data_pegawai.id_jabatan,
            mt_data_pegawai.nama,
            mt_data_pegawai.status,

            mt_data_jabatan.nama_jabatan,
        ");
        $this->db->from("mt_data_pegawai");
        $this->db->join("mt_data_jabatan", "mt_data_pegawai.id_jabatan = mt_data_jabatan.id_jabatan");
        
        $query = $this->db->get();

        return $query->result();
    }

    public function get_mail(){
        $this->db->select("
            ut_smtp.id,
            ut_smtp.protocol,
            ut_smtp.smtp_host,
            ut_smtp.smtp_port,
            ut_smtp.smtp_user,
            ut_smtp.smtp_pass,
        ");
        $this->db->from("ut_smtp");
        
        $query = $this->db->get();

        return $query->result();
    }

}