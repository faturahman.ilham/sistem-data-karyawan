<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="#">
    <title><?= $title." - "."Sistem Data Karyawan" ?></title>
    <!-- Custom CSS -->
    <?php $this->load->view('backend/maincss'); ?>
    <!-- End Custom CSS -->
    <!-- Jquery -->
    <?php $this->load->view('backend/mainjs'); ?>
    <!-- End Jquery -->
</head>

<body class="skin-default fixed-layout">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Sistem Data Karyawan</p>
        </div>
    </div>
    <!-- Main wrapper - style you can find in pages.scss -->
    <div id="main-wrapper">
        <!-- Topbar header - style you can find in pages.scss -->
        <?php $this->load->view('backend/nav'); ?>
        <!-- End Topbar header -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <?php $this->load->view('backend/sidebar'); ?>
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Content -->
                <?php $this->load->view($content); ?>
                <!-- End Content -->
                <!-- Right sidebar -->
                <!-- .right-sidebar -->
                <?php $this->load->view('backend/setting'); ?>
                <!-- End Right sidebar -->
            </div>
            <!-- End Container fluid  -->
        </div>
        <!-- End Page wrapper  -->
        <!-- footer -->
        <footer class="footer">
            COPYRIGHT © 2022 <a href="<?= site_url() ?>">Sistem Data Karyawan</a>
        </footer>
        <!-- End footer -->
    </div>
    <!-- End Wrapper -->
</body>

</html>