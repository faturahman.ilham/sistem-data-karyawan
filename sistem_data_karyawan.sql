-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.24-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for sistem_data_karyawan
CREATE DATABASE IF NOT EXISTS `sistem_data_karyawan` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `sistem_data_karyawan`;

-- Dumping structure for table sistem_data_karyawan.mt_data_jabatan
CREATE TABLE IF NOT EXISTS `mt_data_jabatan` (
  `id_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `UserAdd` varchar(50) DEFAULT NULL,
  `UserCh` varchar(50) DEFAULT NULL,
  `DateAdd` datetime DEFAULT NULL,
  `DateCh` datetime DEFAULT NULL,
  PRIMARY KEY (`id_jabatan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sistem_data_karyawan.mt_data_jabatan: ~0 rows (approximately)
/*!40000 ALTER TABLE `mt_data_jabatan` DISABLE KEYS */;
INSERT IGNORE INTO `mt_data_jabatan` (`id_jabatan`, `nama_jabatan`, `status`, `UserAdd`, `UserCh`, `DateAdd`, `DateCh`) VALUES
	(1, 'jabatan1', 0, NULL, NULL, '2022-05-30 11:22:17', NULL),
	(2, 'jabatan2', 0, NULL, NULL, '2022-05-30 11:22:17', NULL),
	(3, 'jabatan3', 0, NULL, NULL, '2022-05-30 11:22:17', NULL),
	(4, 'jabatan4', 0, NULL, NULL, '2022-05-30 11:22:17', NULL),
	(5, 'jabatan5', 1, NULL, NULL, '2022-05-30 11:22:17', '2022-05-30 11:23:12');
/*!40000 ALTER TABLE `mt_data_jabatan` ENABLE KEYS */;

-- Dumping structure for table sistem_data_karyawan.mt_data_kontrak
CREATE TABLE IF NOT EXISTS `mt_data_kontrak` (
  `id_kontrak` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` int(11) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `awal_kontrak` date DEFAULT NULL,
  `akhir_kontrak` date DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `UserAdd` varchar(50) DEFAULT NULL,
  `UserCh` varchar(50) DEFAULT NULL,
  `DateAdd` datetime DEFAULT NULL,
  `DateCh` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kontrak`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sistem_data_karyawan.mt_data_kontrak: ~0 rows (approximately)
/*!40000 ALTER TABLE `mt_data_kontrak` DISABLE KEYS */;
INSERT IGNORE INTO `mt_data_kontrak` (`id_kontrak`, `id_pegawai`, `id_jabatan`, `awal_kontrak`, `akhir_kontrak`, `status`, `UserAdd`, `UserCh`, `DateAdd`, `DateCh`) VALUES
	(1, 5, NULL, '2022-05-30', '2023-05-30', 1, NULL, NULL, '2022-05-30 11:23:12', NULL);
/*!40000 ALTER TABLE `mt_data_kontrak` ENABLE KEYS */;

-- Dumping structure for table sistem_data_karyawan.mt_data_pegawai
CREATE TABLE IF NOT EXISTS `mt_data_pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `UserAdd` varchar(50) DEFAULT NULL,
  `UserCh` varchar(50) DEFAULT NULL,
  `DateAdd` datetime DEFAULT NULL,
  `DateCh` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pegawai`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sistem_data_karyawan.mt_data_pegawai: ~0 rows (approximately)
/*!40000 ALTER TABLE `mt_data_pegawai` DISABLE KEYS */;
INSERT IGNORE INTO `mt_data_pegawai` (`id_pegawai`, `nama`, `id_jabatan`, `status`, `UserAdd`, `UserCh`, `DateAdd`, `DateCh`) VALUES
	(1, 'pegawai1', 1, 0, NULL, NULL, '2022-05-30 11:22:17', NULL),
	(2, 'pegawai2', 2, 0, NULL, NULL, '2022-05-30 11:22:17', NULL),
	(3, 'pegawai3', 3, 0, NULL, NULL, '2022-05-30 11:22:17', NULL),
	(4, 'pegawai4', 4, 0, NULL, NULL, '2022-05-30 11:22:17', NULL),
	(5, 'pegawai5', 5, 1, NULL, NULL, '2022-05-30 11:22:17', '2022-05-30 11:23:12');
/*!40000 ALTER TABLE `mt_data_pegawai` ENABLE KEYS */;

-- Dumping structure for table sistem_data_karyawan.mt_mail
CREATE TABLE IF NOT EXISTS `mt_mail` (
  `id_email` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(50) DEFAULT NULL,
  `isi` text DEFAULT NULL,
  `to` varchar(50) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `UserAdd` varchar(50) DEFAULT NULL,
  `DateAdd` datetime DEFAULT NULL,
  `UserCh` varchar(50) DEFAULT NULL,
  `DateCh` datetime DEFAULT NULL,
  PRIMARY KEY (`id_email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sistem_data_karyawan.mt_mail: ~0 rows (approximately)
/*!40000 ALTER TABLE `mt_mail` DISABLE KEYS */;
INSERT IGNORE INTO `mt_mail` (`id_email`, `subject`, `isi`, `to`, `date`, `status`, `UserAdd`, `DateAdd`, `UserCh`, `DateCh`) VALUES
	(1, 'lorem ipsum', '<p><strong style="margin: 0px; padding: 0px; font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Lorem Ipsum</strong><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><br></p>', 'faturahman.ilham@gmail.com', '2022-05-30 11:30:48', 1, NULL, '2022-05-30 11:30:48', NULL, NULL);
/*!40000 ALTER TABLE `mt_mail` ENABLE KEYS */;

-- Dumping structure for table sistem_data_karyawan.ut_smtp
CREATE TABLE IF NOT EXISTS `ut_smtp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protocol` varchar(50) DEFAULT NULL,
  `smtp_host` varchar(50) DEFAULT NULL,
  `smtp_port` varchar(50) DEFAULT NULL,
  `smtp_user` varchar(50) DEFAULT NULL,
  `smtp_pass` varchar(50) DEFAULT NULL,
  `UserAdd` varchar(50) DEFAULT NULL,
  `DateAdd` datetime DEFAULT NULL,
  `UserCh` varchar(50) DEFAULT NULL,
  `DateCh` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table sistem_data_karyawan.ut_smtp: 1 rows
/*!40000 ALTER TABLE `ut_smtp` DISABLE KEYS */;
INSERT IGNORE INTO `ut_smtp` (`id`, `protocol`, `smtp_host`, `smtp_port`, `smtp_user`, `smtp_pass`, `UserAdd`, `DateAdd`, `UserCh`, `DateCh`) VALUES
	(1, 'smtp', 'ssl://smtp.gmail.com', '465', 'admin@gmail.com', '123456789', NULL, NULL, NULL, '2022-05-30 11:47:25');
/*!40000 ALTER TABLE `ut_smtp` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
