<?php
defined('BASEPATH') OR exit('No direct script access allowed');
#bahasa indonesia
$lang['mediasosial'] = 'Bersosialisasi dengan kami';
$lang['tentang'] = 'TENTANG BURLIBUR';
$lang['kenapa'] = 'Kenapa Burlibur ?';
$lang['popular_des'] = 'Tempat Tujuan Populer';
$lang['top_attraction']	= 'Atraksi Terpopuler';
$lang['top_diaspora'] = 'Diaspora Terpopuler';
$lang['recomend_att'] = 'Terekomendasi';
$lang['top_singapore'] = 'Tempat Rekreasi Populer di Singapore';
$lang['top_hongkong'] = 'Tempat Rekreasi Populer di Hongkong';
$lang['top_korea'] = 'Tempat Rekreasi Populer di Korea';
$lang['top_japan'] = 'Tempat Rekreasi Populer di Jepang';
$lang['pay_ch'] = 'SALURAN PEMBAYARAN';
#halaman login
$lang['login'] 	= 'Masuk';
$lang['loginfb'] = 'Masuk dengan Facebook';
$lang['logingoogle'] = 'Masuk dengan Google';
$lang['or'] = 'ATAU';
$lang['forgotpass'] = 'Lupa Password ?';
$lang['pass'] = 'Kata Sandi';
$lang['regnow'] = 'Daftar Sekarang';
#halaman register
$lang['reg'] = 'Daftar';
$lang['regfb'] = 'Daftar dengan Facebook';
$lang['reggoogle'] = 'Daftar dengan Google';
$lang['gelar'] = 'Gelar';
$lang['lastname'] = 'Nama Belakang';
$lang['firstname'] = 'Nama Depan';
$lang['email'] = 'Alamat Email';
$lang['countrycode'] = 'Kode Negara';
$lang["infopassport"] = "Informasi paspor";

#halaman depan
$lang["pure_price"] = "Harga Jujur";
$lang["pure_priced"] = "Tidak ada biaya tambahan, harga tertera adalah harga yang anda bayarkan Kurs dalam Rupiah, Tidak ada selisih kurs mata uang";
$lang["pay_safe"] = "Pembayaran Aman & Mudah";
$lang["pay_safed"] = "Pembayaran Aman & Mudah Banyak metode pembayaran Dijamin aman & mudah";
$lang["complete_info"] = "Informasi Lengkap";
$lang["complete_infod"] = "Informasi Lengkap Informasi comprehensive mengenai atraksi wisata Stress free merencanakan liburan Anda";
$lang["diverse_option"] = "Beragam Pilihan";
$lang["diverse_optiond"] = "Beragam pilihan Satu situs dengan banyak pilihan wisata yang telah dipilih sesuai untuk wisatawan Indonesia";

#string
$lang["info"] = "Informasi";
$lang["tripsuitable"] = "Apakah Trip Ini Cocok Untuk Anda ?";
$lang["tersedia"] = "Hari Ini Tersedia";
$lang["tersediax"] = "Hari Ini Tidak Tersedia";
$lang["konfirmasi48"] = "48 Jam Konfirmasi";
$lang["konfirmasijam"] = "Jam Konfirmasi";

$lang["konfirmasi"] = "Konfirmasi";
$lang["termasuk"] = "Termasuk";
$lang["termasukx"] = "Tidak Termasuk";
$lang["carapakai"] = "Cara Penggunaan";
$lang["howtogetthere"] = "Cara Menuju ke Sana";
$lang["tips"] = "Saran";
$lang["infotambah"] = "Informasi Tambahan";
$lang["dapatharapan"] = "Apa Yang Dapat Anda Harapkan";
$lang["infokegiatan"] = "Informasi Kegiatan";
$lang["kebijakanbatal"] = "Kebijakan Pembatalan";
$lang["voucherisue"] = "Waktu Penerbitan Voucher";
$lang["pilihpaket"]= "Pilih Paket";
$lang["ulasan"]= "Ulasan";
$lang["book"] = "Pesan";
$lang["book_now"] = "Pesan Sekarang";
$lang["book_detail"] = "Rincian Pesanan";
$lang["dewasa"] = "Dewasa";
$lang["anak"] = "Anak-anak";
$lang["lansia"] = "Lansia";
$lang["namapaket"] = "Nama Paket";
$lang["tgl"] = "Tanggal";
$lang["potongan"] = "Potongan";
$lang["pay_made"] = "Transaksi anda akan dilakukan dalam IDR";
$lang["pay_maded"] = "Saat ini kami hanya menerima transaksi dalam rupiah. Mata uang yang tampil lainnya yang Anda pilih akan dikenakan biaya dalam IDR. Burlibur tidak membebankan biaya layanan atau biaya tambahan. Jika karena alasan tertentu Anda melihat biaya tambahan, silakan cek dengan penerbit kartu kredit Anda.";
$lang["paynow"] = "Bayar Sekarang";

// MW
#profile member frontend
$lang["my_account"] 	= "Akun Saya";
$lang["booking"]		= "Pemesanan";
$lang["reviews"]		= "Ulasan";
$lang["rewards"]		= "Reward";
$lang["setting"]		= "Pengaturan";
$lang["logout"]			= "Keluar";
$lang["change_picture"]	= "Ubah Gambar";
$lang["gender"]			= "Jenis Kelamin";
$lang["day_of_birth"]	= "Tanggal Lahir";
$lang["address"]		= "Alamat";
$lang["country"]		= "Negara";
$lang["city"]			= "Kota";
$lang["phone"]			= "No Telepon";
$lang["save"]			= "Simpan";
$lang["male"]			= "Laki-Laki";
$lang["female"]			= "Perempuan";
$lang["select_city"]	= "Pilih Kota";
$lang["select_country"] = "Pilih Negara";
$lang["account_info"]	= "Informasi Profile";
$lang["bank_info"]		= "Informasi Bank";
$lang["bank_name"]		= "Nama Bank";
$lang["bank_account_name"] = "Nama Akun";
$lang["bank_account_no"]   = "Nomor Rekening";
$lang["transaction_code"]  = "No Transaksi";
$lang["review"]			   = "Ulasan";
$lang["transaction_status"]= "Status Transaksi";
$lang["date_start"]		= "Tanggal Mulai";
$lang["date_end"]		= "Tanggal Ahkir";
$lang["date_select"]	= "Masukan Tanggal";
$lang["search"]			= "Cari";
$lang["reload"]			= "Reload";
$lang["book_not_found"]	= "Pemesanan tidak ditemukan";
$lang["transaction_not"]= "Anda belum melakukan transaksi, silahkan melakukan transaksi";
$lang["transaction_start"] = "Mulai Transaksi";
$lang["review_not"]		= "Tidak ada ulasan, silahkan melakukan transaksi";
$lang["activity"]		= "Aktivitas";
$lang["saldo"]			= "Saldo";
$lang["used"]			= "Digunakan";
$lang["total_earning"]	= "Jumlah Pendafatan";
$lang["current_pass"]	= "Kata Sandi Lama";
$lang["new_pass"]		= "Kata Sandi Baru";
$lang["confirm_pass"]	= "Konfirmasi Kata Sandi";
$lang["length_pass"]	= "(kata sandi hrus lebih dari 6 karakter)";
$lang["check_form"]		= "Mohon periksa form Anda";
$lang["success"]		= "Berhasil";
$lang["failed"]			= "Gagal";
$lang["all"]			= "Semua";
$lang["close"]			= "Keluar";
$lang["back"]			= "Kembali";
$lang["ya"]				= "Iya";
$lang["no"] 			= "Tidak";

#validation
$lang["pass_enter"]		= "masukan kata sandi";
$lang["pass_not_match"]	= "kata sandi tidak cocok";
$lang["pass_old_incr"]	= "kata sandi lama salah";

#cancellation
$lang["payment_method"]	= "Metode Pembayaran";
$lang["refund_reason"]	= "Alasan Refund";
$lang["refund_detail"]	= "Rincian Refund";
$lang["transaction_details"] = "Rincian Transaksi";
$lang["tanya_dana"]		= "Apakah Pengembalian Dana Anda ingin ditransfer ke informasi bank di atas?";

#footer
$lang["about_us"]		= "Tentang Kami";
$lang["privacy_policy"]	= "Kebijakan Privasi";
$lang["terms"]			= "Syarat dan Ketentuan";

#inclusions
$lang["inclusions"] 		= "Termasuk";
$lang["meals"] 				= "Makan";
$lang["transport"] 			= "Kendaraan";
$lang["accomodation"] 		= "Akomodasi";
$lang["includedactivities"] = "Kegiatan";

$lang["open_day"]	= "Hari Operasional";
$lang["open_time"]	= "Jam Operasional";


$lang["service"]	= "Servis";
$lang["medical"]	= "Medis";
$lang["contact_us"]	= "Hubungi Kami";
$lang["news_event"] = "News And Events";
$lang["insurance"] 	= "Asuransi";
$lang["p_residency"] = "Residensi Permanen";
$lang["business_op"] = "Peluang Bisnis dan Investasi";
$lang["accomodation"] = "Akomodasi";
$lang["medical_evacuation"] = "Evakuasi Medis";
$lang["concierge"] = "Jasa Pendampingan";
$lang["medical_app"] = "Janji Konsul Dokter";
$lang["medical_app_req"] = "Meminta Janji Konsul";

$lang["nama_pasien"] = "Nama Pasien";
$lang["tgl_lahir"] = "Tanggal Lahir";
$lang["passport_no"] = "No Passpor";
$lang["passport_img"] = "Gambar Passpor";
$lang["contact_no"]	= "No Whatsapp";
$lang["medical_case"]	= "Keluhan medis dan kondisi";
$lang["req_doctor"]	= "Nama dokter yang diinginkan";
$lang["hospital_clinic"]	= "Rumah Sakit / Klinik";
$lang["visit_date"]	= "Rencana kedatangan";

$lang["specialist"] = "Dokter Spesialis di Singapura,<br/> Malaysia dan Korea";
$lang["hospitals"] = "Rumah Sakit di Singapura,<br/> Malaysia dan Korea";

$lang["name"] = "Nama";
$lang["email"]	= "Alamat Email";
$lang["wano"]	= "Nomor Whatsapp";
$lang["subject"] = "Subjek";
$lang["message"]	= "Pesan";
$lang["sendmessage"]	= "Kirim Pesan";
$lang["send"]	= "Kirim";
$lang["close"]	= "Tutup";

$lang["send_contact"] = "Kirim kami pesan";
$lang["text_contact"] = "Kami senang mendengar dari Anda. <br/>Hubungi kami, melalui";

$lang["text_slide1"] = "Halo, ada yang<br/> dapat kami bantu ?";
$lang["text_slide2"] = "“Saya percaya bahwa pemberian terbesar yang dapat Anda berikan kepada keluarga Anda dan dunia adalah kesehatan Anda” – Joyce Meyer";
$lang["text_slide3"] = "“Jangan membiasakan diri utk menunggu. Hidupilah mimpimu dan ambil resiko. Karena hidup ini terjadi sekarang !”";

$lang['singapura'] = "Singapura";
$lang['email_t'] = "Email";
$lang['ex'] = "Contoh";