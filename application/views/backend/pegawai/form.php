<?php
    $get_jabatan = $this->api->jabatan();  
?>

<form id="form" autocomplete="off">
    <div class="row form-view content-hide">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-primary">
                    <h4 class="m-b-0 pg-title text-white"></h4>
                </div>
                <div class="card-body">
                    <input type="hidden" name="crud">
                    <input type="hidden" name="ID">
                    <input type="hidden" name="page_url">
                    <input type="hidden" name="page_module">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Nama<span class="wajib"></span></label>
                                <input type="text" id="Nama" name="Nama" class="form-control">
                                <small class="form-control-feedback"></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Jabatan<span class="wajib"></span></label>
                                <select class="form-control" name="jabatan">
                                <option value="none">--------Pilh Jabatan--------</option>
                                <?php  foreach ($get_jabatan as $v){ ?>
                                    <option value="<?= $v->ID ?>"><?= $v->nama_jabatan ?></option>
                                <?php }?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <?= $this->main->button('action') ?>
                </div>
            </div>
        </div>
    </div>
</form>
