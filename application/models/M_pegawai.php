<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class M_pegawai extends CI_Model {
	var $table = 'mt_data_pegawai';
	var $column = array("mt_data_pegawai.id_pegawai","mt_data_pegawai.nama","mt_data_pegawai.id_jabatan","mt_data_pegawai.status"); //set column field database for order and search
	var $order = array('mt_data_pegawai.id_pegawai' => 'desc'); // default order 

	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    private function _get_datatables_query()
	{
		$table 			= $this->table;
		$searchx 		= $this->input->post('Searchx');

		$this->db->select("
			$table.id_pegawai,
			$table.id_jabatan,
			$table.nama,
			$table.status,

			mt.nama_jabatan
		");
		$this->db->from($this->table);
		$this->db->join("mt_data_jabatan as mt", "mt.id_jabatan = $table.id_jabatan", "left");

		$i = 0;
	
		foreach ($this->column as $item) // loop column 
		{
			if($searchx) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND. 
					$this->db->like($item, $searchx);
				}
				else
				{
					$this->db->or_like($item, $searchx);
				}

				if(count($this->column) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$column[$i] = $item; // set column array variable to order processing
			$i++;
		}
		
		if($this->input->post('order')) // here order processing
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($this->input->post('length') != -1)
		$this->db->limit($this->input->post('length'), $this->input->post('start'));
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$HakAksesType   = $this->session->HakAksesType;
		$CompanyID 		= $this->session->CompanyID;
		$Companyx 		= $this->input->post('Companyx');
		$searchx 		= $this->input->post('Searchx');

		if(in_array($HakAksesType, array(1,2))):
			$CompanyID = $Companyx;
		endif;

		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($p1){

		$table 			= $this->table;

		$this->db->select("
			mt_data_pegawai.id_pegawai as ID,
			mt_data_pegawai.id_jabatan,
			mt_data_pegawai.nama,
			mt_data_pegawai.status,
			
			mt.nama_jabatan,
		");
		$this->db->from($this->table);
		$this->db->join("mt_data_jabatan as mt", "mt.id_jabatan = $table.id_jabatan", "left");
		$this->db->where("mt_data_pegawai.id_pegawai", $p1);
		
		$query = $this->db->get();

		return $query->row();
	}

	public function export($p1=""){

		if($p1 == 'template'):
			$title 			= "Template-Data-Pegawai";
			$file_name 		= $title.date("Ymd_His").".csv";
			$lap_data   	= $this->api->get_pegawai("export");
		endif;

		// echo "<pre>";
		// print_r($lap_data); exit();
		// echo "</pre>";
		
		if($p1 == 'template'):
			$spreadsheet = new Spreadsheet();
			$sheet 		 = $spreadsheet->getActiveSheet();
			
			$sheet->setCellValue('A1', 'No');

			$sheet->setCellValue('B1', 'Pegawai');
			$sheet->setCellValue('C1', 'Jabatan');

			$no = 1;
			$x 	= 2;
			foreach($lap_data as $row)
			{
				$sheet->setCellValue('A'.$x, $no++);
				$sheet->setCellValue('B'.$x, $row->nama);
				$sheet->setCellValue('C'.$x, $row->nama_jabatan);
				$x++;
			}		
		endif;

		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Csv($spreadsheet);
		$writer->setUseBOM(true);
		$writer->setDelimiter(',');
		$writer->setEnclosure('');
		$writer->setLineEnding("\r\n");
		$writer->setSheetIndex(0);	

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$file_name.'"'); 
		header('Cache-Control: max-age=0');
		
		$writer->save('php://output');
	}
}