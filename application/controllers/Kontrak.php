<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontrak extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model("M_kontrak","kontrak");
    }

    public function index(){
    	$this->main->check_session();
		$url 				= $this->uri->segment(1);
		$module 			= "kontrak";
		$MenuID 			= "kontrak";
		$read_menu 			= $this->api->read_menu($MenuID);
		if($read_menu->view<=0): redirect('403_override'); endif;
		$btn_add = '';
		$btn_import = '';
		if($read_menu->insert>0): $btn_add = $this->main->button('add_new'); $btn_import = 'import'; endif;

		$data["title"] 		= "Master Data kontrak";
		$data['url']		= $url;
		$data['module']		= $module;
		$data['content'] 	= 'backend/kontrak/list';
		$data['form']		= 'backend/kontrak/form';
		$data['filter']		= 'backend/kontrak/filter';
		$data['import']		= 'backend/kontrak/import';
		$data['btn_add']	= $btn_add;
		$data['btn_import']	= $btn_import;
		$this->load->view('backend/index',$data);
	}

	public function list(){
		$data 	= array();
		$list 	= $this->kontrak->get_datatables();
		$i 		= $this->input->post('start');

		$url 		= $this->input->post('page_url');
		$module 	= $this->input->post('page_module');
		$MenuID 	= "kontrak";
		$read_menu 	= $this->api->read_menu($MenuID);

		foreach ($list as $a):
			$no = $i++;
			$no += 1;
			$txt_status = $this->main->label_active($a->status);
			$tg_data = ' data-id="'.$a->id_kontrak.'" ';
			$tg_data .= ' data-status="'.$a->status.'" ';

			$btn_array = array();
			$btn_status = false;
			// if($read_menu->update>0):
			// 	$btn_status = true;
			// 	array_push($btn_array, 'edit');
			// endif;
			// if($read_menu->delete>0):
			// 	$btn_status = true;
			// 	array_push($btn_array, 'delete');
			// endif;

			if($btn_status):
				$btn = $this->main->button('action_list',$a->id_kontrak,$btn_array);
			else:
				$btn = '';
			endif;
			$temp = '<span class="dt-id" '.$tg_data.'>'.$no.'</span>';
			
			$row = array();
			$row[] 	= $btn.$temp;
			$row[] 	= $a->nama_pegawai;
			$row[] 	= $this->main->tanggal_indo($a->awal_kontrak);
			$row[] 	= $this->main->tanggal_indo($a->akhir_kontrak);
			$row[] 	= $this->main->label_background($txt_status);
			$data[] = $row;
		endforeach;

		$response = array(
			"draw" 				=> $this->input->post('draw'),
			"recordsTotal" 		=> $this->kontrak->count_all(),
			"recordsFiltered" 	=> $this->kontrak->count_filtered(),
			"data" 				=> $data,
		);

		$this->main->echoJson($response);
	}

	public function save(){
		$url 				= $this->input->post('page_url');
		$module 			= $this->input->post('page_module');
		$MenuID 			= "kontrak";
		$read_menu 			= $this->api->read_menu($MenuID);

		$Crud 		= $this->input->post("crud");
		if($Crud == "insert"): $p3 = $read_menu->insert;
		elseif($Crud == "update"): $p3 = $read_menu->update;
		else: $p3 = 0;
		endif;

		$this->main->check_session('out',$Crud, $p3);
		// $this->validation->leave();

		$ID 		     = $this->input->post("ID");
		$pegawai 	     = $this->input->post("pegawai");
		$awal            = $this->input->post("awal");
		$akhir           = $this->input->post("akhir");

		$data = array(
			"id_pegawai"  	    => $pegawai,
			"awal_kontrak"  	=> $awal,
			"akhir_kontrak"  	=> $akhir,
			"status"			=> 1,
		);

		$get_pegawai = $this->db->query("SELECT * FROM mt_data_pegawai WHERE id_pegawai = '$pegawai'")->result();

		$update_status	= array(
			"status"	=> 1,
		);

		if($Crud == "insert"):
			$message = "Insert Success";
			$this->main->general_save("mt_data_kontrak", $data);
			$this->main->general_update("mt_data_pegawai", $update_status, array("id_pegawai" => $get_pegawai[0]->id_pegawai));
			$this->main->general_update("mt_data_jabatan", $update_status, array("id_jabatan" => $get_pegawai[0]->id_jabatan));
		elseif($Crud == "update"):
			$message =  "Update Success";
			$this->main->general_update("mt_data_kontrak", $data, array("id_kontrak" => $ID));
		endif;

		$response = array(
			"status"	=> true,
			"message"	=> $message,
		);
		$this->main->echoJson($response);
	}

	public function edit(){
		$url 				= $this->input->post('page_url');
		$module 			= $this->input->post('page_module');
		$MenuID 			= "kontrak";
		$read_menu 			= $this->api->read_menu($MenuID);
		$this->main->check_session('out', 'update', $read_menu->update);
		$ID 		= $this->input->post('ID');
		$list 		= $this->kontrak->get_by_id($ID);

		$response = array(
			"status"	=> true,
			"data"		=> $list,
			"message"	=> "Success",
		);

		$this->main->echoJson($response);
	}

	public function delete(){
		$url 				= $this->input->post('page_url');
		$module 			= $this->input->post('page_module');
		$MenuID 			= "kontrak";
		$read_menu 			= $this->api->read_menu($MenuID);

		$this->main->check_session('out', 'delete', $read_menu->delete);
		$ID = $this->input->post('ID');

		$this->db->where("id_kontrak", $ID);
		$this->db->delete("mt_data_kontrak");

		$response = array(
			"status"	=> true,
			"message"	=> "Success",
		);

		$this->main->echoJson($response);
	}

}