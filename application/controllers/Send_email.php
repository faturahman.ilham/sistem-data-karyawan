<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Send_email extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model("M_email","send_email");
    }

    public function index(){
		$url 				= $this->uri->segment(1);
		$module 			= "send_email";

		$data["title"] 		= "Send E-mail";
		$data['url']		= $url;
		$data['module']		= $module;
		$data['content'] 	= 'backend/send_email/list';
		$this->load->view('backend/index',$data);
	}

	public function list(){
		$data 	= array();
		$list 	= $this->send_email->get_datatables();
		$i 		= $this->input->post('start');

		$url 		= $this->input->post('page_url');
		$module 	= $this->input->post('page_module');
		$MenuID 	= 1;
		$read_menu 	= $this->api->read_menu($MenuID);

		foreach ($list as $a):
			$no = $i++;
			$no += 1;
			$txt_status = $this->main->label_kirim($a->status);
			$tg_data = ' data-id="'.$a->id_email.'" ';
			$tg_data .= ' data-status="'.$a->status.'" ';

			$btn_array = array();
			$btn_status = false;
			if($a->status == 0 and $read_menu->update>0):
				$btn_status = true;
				array_push($btn_array, 'edit');
			endif;
			if($a->status == 0 and $read_menu->delete>0):
				$btn_status = true;
				array_push($btn_array, 'delete');
			endif;

			if($btn_status):
				$btn = $this->main->button('action_list',$a->id_email,$btn_array);
			else:
				$btn = '';
			endif;
			$temp = '<span class="dt-id" '.$tg_data.'>'.$no.'</span>';
			
			$row = array();
			$row[] 	= $btn.$temp;
			$row[] 	= $a->subject;
			$row[] 	= $a->to;
			$row[] 	= $a->date;
			$row[] 	= $this->main->label_background_kirim($txt_status);
			$data[] = $row;
		endforeach;

		$response = array(
			"draw" 				=> $this->input->post('draw'),
			"recordsTotal" 		=> $this->send_email->count_all(),
			"recordsFiltered" 	=> $this->send_email->count_filtered(),
			"data" 				=> $data,
		);

		$this->main->echoJson($response);
	}

    public function save(){
		$url 				= $this->input->post('page_url');
		$module 			= $this->input->post('page_module');
		$MenuID 			= 1;
		$read_menu 			= $this->api->read_menu($MenuID);

		$Crud 		= $this->input->post("crud");
		if($Crud == "insert"): $p3 = $read_menu->insert;
		elseif($Crud == "update"): $p3 = $read_menu->update;
		else: $p3 = 0;
		endif;

		$this->main->check_session('out',$Crud, $p3);
		$this->validation->mail();

		$ID 		= $this->input->post("ID");
		$to 		= $this->input->post("to");
		$subject 	= $this->input->post("subject");
		$isi 	    = $this->input->post("isi");

		$data = array(
            "to"  	    => $to,
			"subject"  	=> $subject,
			"isi"  	    => $isi,
			"date"  	=> date('Y-m-d H:i:s'),
        	"status"	=> 1,
		);

        $message = "Send Email Success";
        $this->main->general_save("mt_mail", $data);
        $this->main->send_email("send",$data);

		$response = array(
			"status"	=> true,
			"message"	=> $message,
		);
		$this->main->echoJson($response);
	}

}