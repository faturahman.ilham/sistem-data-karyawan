<?php
    $get_pegawai = $this->api->pegawai();  
?>

<form id="form" autocomplete="off">
    <div class="row form-view content-hide">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-primary">
                    <h4 class="m-b-0 pg-title text-white"></h4>
                </div>
                <div class="card-body">
                    <input type="hidden" name="crud">
                    <input type="hidden" name="ID">
                    <input type="hidden" name="page_url">
                    <input type="hidden" name="page_module">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Nama Pegawai<span class="wajib"></span></label>
                                <select class="form-control" name="pegawai">
                                <option value="none">--------Pilh Pegawai--------</option>
                                <?php  foreach ($get_pegawai as $v){ ?>
                                    <option value="<?= $v->ID ?>"><?= $v->nama ?></option>
                                <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Tanggal Kontrak<span class="wajib"></span></label>
                                <input type="text" id="awal" name="awal" value="<?= date("Y-m-d") ?>" class="form-control mdate" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Berakhir kontrak<span class="wajib"></span></label>
                                <input type="text" id="akhir" name="akhir" value="<?= date('Y-m-d', strtotime('+1 year')) ?>" class="form-control mdate" readonly>
                            </div>
                        </div>
                    </div>

                    <?= $this->main->button('action') ?>
                </div>
            </div>
        </div>
    </div>
</form>
