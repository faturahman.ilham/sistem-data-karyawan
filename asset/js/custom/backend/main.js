var mobile 		= (/iphone|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));  
var host 		= window.location.origin+'/sistem_data_karyawan/';
var url_page 	= window.location.href;
var url_serverSide  = host+"api/serverSide";
var time_long_click = 1000;
var img_default = host+"img/image_default.png";
$(document).ready(function() {
    plugin();
    CheckTheme();
});


function plugin(){
	if($('span').hasClass('wajib')){
		$('span.wajib').addClass('rd-color').text('*');
	}

	 if ($("input,textarea").hasClass("summernote")) {
	    summernote_init();
	 }

	if($('div').hasClass('show_password')){
		$('.show_password').on('click',function(){
			check_show_password(this);
		});
	}


	if($('input').hasClass('time-format')){
		$('.time-format').formatter({'pattern': '{{99}}:{{99}}', });
	}

	if($('input').hasClass('datetime-format')){
		$('.datetime-format').formatter({'pattern' : '{{9999}}-{{99}}-{{99}} {{99}}:{{99}}',})
	}

	$( ".v-themes" ).click(function() {
	  SetThemes(this);
	});
}



function open_form(p1,p2){
	$('html, body').animate({
		scrollTop : $('.page-data').offset().top - 100
	}, 'fast');
	if(p1 == 'close'){
		$('.form-view, .form-import').hide(300);
		$('.list-view').show(300);
	}else if(p1 == 'edit'){
		$('.form-view input').attr('disabled', false);
		$('.form-view').removeClass('content-hide');
		$('.form-view .pg-title').text('Edit Data');
		$('.form-view').show(300);
		$('.list-view, .form-import').hide(300);
		$('.v-Save').show();
		$('.v-Approved').hide();
		edit_data(p2);
	}else if(p1 == 'view'){
		$('.form-view input').attr('disabled', true);
		$('.form-view').removeClass('content-hide');
		$('.form-view .pg-title').text('View Data');
		$('.form-view').show(300);
		$('.list-view, .form-import').hide(300);
		$('.v-Save').hide();
		$('.v-Approved').show();
		edit_data(p2);
	}else if(p1 == 'import'){
		$('.v-import-result').hide();
		$('.form-import').removeClass('content-hide');
		$('.form-import .pg-title').text('Import Data');
		$('.form-view, .list-view').hide(300);
		$('.form-import').show(300);
		$('#form-import')[0].reset();
		$('#form-import .form-control-feedback').text('');
		$('#form-import .has-danger').removeClass('has-danger');
	    $('#form-import .select2-has-danger').removeClass('select2-has-danger');
	    $('.dropify-clear').click();
	    $('#table-import-result thead').empty();
    	$('#table-import-result tbody').empty();
    	$('.import-total-succeess').empty();
    	$('.import-total-failed').empty();
    	$('.import-total').empty();
	}else{
		$('.form-view input').attr('disabled', false);
		$('.form-view').removeClass('content-hide');
		$('.form-view .pg-title').text('Add Data');
		$('.form-view').show(300);
		$('.list-view, .form-import').hide(300);
		$('.v-Save').show();
		$('.v-Approved').hide();
		add_data();
	}
}

function proccess_save(){
	$('.btn-save').button('loading');
	$('button').attr('disabled', true);
}

function end_save(){
	$('.btn-save').button('reset');
	$('button').attr('disabled', false);	
}
function session_expired(url){
	swal({
	    title: "",
	    text: "Session Expired",
	    type: "success"
	}, function() {
	    window.location = url;
	});
}

function show_console(p1){
	console.log(p1);
}

function check_show_password(p1){
	tg_data = $(p1).data();
	if(tg_data.status){
		$(p1).prev().attr('type', 'password');
		$(p1).find('.btn-icon').removeClass('fa-eye-slash');
		$(p1).find('.btn-icon').addClass('fa-eye');
		$(p1).data('status','');
	}else{
		$(p1).prev().attr('type', 'text');
		$(p1).find('.btn-icon').removeClass('fa-eye');
		$(p1).find('.btn-icon').addClass('fa-eye-slash');
		$(p1).data('status','1');
	}
}

function show_invalid_response(data){
	if(data.session){
        session_expired(data.url);
    }else{
        swal('',data.message,'warning');
        if(data.inputerror){
        	for (var i = 0; i < data.inputerror.length; i++)
	        {
	            input_type = data.input_type[i];
	            if(input_type == "select2"){
	                $('.'+data.inputerror[i]+'-view').addClass('select2-has-danger');
	                $('.'+data.inputerror[i]+'-view .form-control-feedback').text(data.error_string[i]);
	            }else if(input_type == "input_group"){
	            	$('.'+data.inputerror[i]+'-view').addClass('has-danger');
	            	$('.'+data.inputerror[i]+'-view .form-control-feedback').text(data.error_string[i]);
	            }else if(input_type == 'array_group'){
	            	$('.'+data.inputerror[i]).eq(data.error_string[i]).addClass('border-red-validate');
	            }else if(input_type == 'input_select'){
	            	$('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-danger');
	            	$('.'+data.inputerror[i]+'-view .form-control-feedback').text(data.error_string[i]);
	            }else{
	                $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-danger');
	                $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
	            }
	        }
        }
    }
}

function clear_value(p1,p2){
	$(p1).val('');
	if(p2 == "icon"){
		set_fa_icon();
	}else if("select_api"){
		$(p1+"-Name").val('');
	}
}

function add_data_page(xform){
	page_data   = $(".page-data").data();
    dt_url      = page_data.url;
    dt_module   = page_data.module;

    $(xform+ ' [name=page_url]').val(dt_url);
    $(xform+ ' [name=page_module]').val(dt_module);

}


// import
function import_header(p1){
	item = '<tr>';
	$.each(p1[0],function(k,v){
		item += '<th>'+v+'</th>';
	});
	item += '<th>Status</th>';
	item += '<th class="mwidth-250p">Message</th>';
	item += '</tr>';
	$('#table-import-result thead').append(item);
}


function create_time_picker(){
	$('.timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
}

function create_time_format(){
	$('.time-format').formatter({'pattern': '{{99}}:{{99}}', });
}

// log info
function log_info(){
	url = host+"log-info";
	data_post = {
		p1 : "log_info",
	}
	$.ajax({
        url : url,
        type: "POST",
        data: data_post,
        dataType: "JSON",
        success: function(data)
        {
            show_console(data);
            $('.log-info').empty();
            if(data.length>0){
            	$.each(data,function(k,v){
            		item = v.Content;
            		$('.log-info').append(item);
            	})
            }else{
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            console.log(jqXHR.responseText);
        }
    });

}

function remove_value(inputnya){
  $('[name='+inputnya+']').val('');
}

function remove_value_user(p1){
	$(p1).val('');
	$(p1+'-Name').val('');
}

function check_type_file(p1,p2){
	icon = host+'img/icon/file.png';
    if(p1 == 'docx' || p1 == 'doc'){
        icon = host+'img/icon/word.png';
    }else if(p1 == 'pdf'){
        icon = host+'img/icon/pdf.png';
    }else if(p1 == 'xls' || p1 == 'csv' || p1 == 'xlsx'){
        icon = host+'img/icon/excel.png';
    }else if(p1 == 'png' || type == 'jpg' || p1 == 'jpeg'){
        icon = p2;
    }

    return icon;
}

function redirect_to(url,p2){
	if(p2){
		window.location.replace(url);
	}else{
		window.open(url, '_blank');
	}
}

function summernote_init() {
  $('.summernote').summernote({
    addclass: {
      debug: false,
      classTags: [{
        title: "Button",
        "value": "btn btn-success"
      }, "jumbotron", "lead", "img-rounded", "img-circle", "img-responsive", "btn", "btn btn-success", "btn btn-danger", "text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
    },
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
      ['fontname', ['fontname']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['height', ['height']],
      ['table', ['table']],
      ['insert', ['link', 'picture', 'video', 'hr']],
      ['view', ['codeview']]
    ],
    fontSize: 16,
    height: 500
  });
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkCookie(p1) {
  var user = getCookie(p1);
  if (user != "") {
    alert("Welcome again " + user);
  } else {
    user = prompt("Please enter your name:", "");
    if (user != "" && user != null) {
      setCookie("username", user, 365);
    }
  }
}

function CheckTheme(){
	var themes = getCookie("themes");
	if(themes != ""){
		SetThemesDefault(themes);
	}else{
		SetThemesDefault(0);
	}
}

function SetThemes(element){
	var index = $(".v-themes").index(element);
	setCookie("themes",index,7);
}

function SetThemesDefault(p1){
	$('.v-themes').eq(p1).click();
}