# Technical Test - Sistem Data Karyawan

* * *

## Install PhpSpreadsheet 

```sh
composer require phpoffice/phpspreadsheet
``` 

## Result

### Master Jabatan 
![Result Name Screen Shot][result1-screenshot]

### Master Data Pegawai 
![Result Name Screen Shot][result2-screenshot]

### Import Data Pegawai 
![Result Name Screen Shot][result8-screenshot]

### Master Data Kontrak 
![Result Name Screen Shot][result4-screenshot]

## Setting Gmail 

```sh
https://myaccount.google.com/lesssecureapps
``` 
### And

```sh
https://accounts.google.com/DisplayUnlockCaptcha
``` 

## Result Send E-Mail  

### Send E-mail
![Result Name Screen Shot][result9-screenshot]

### List Success Send E-mail
![Result Name Screen Shot][result5-screenshot]

### Inbox Gmail 
![Result Name Screen Shot][result6-screenshot]


[result1-screenshot]: images/Screenshot_1.png
[result2-screenshot]: images/Screenshot_2.png
[result3-screenshot]: images/Screenshot_3.png
[result4-screenshot]: images/Screenshot_4.png
[result5-screenshot]: images/Screenshot_5.png
[result6-screenshot]: images/Screenshot_6.png

[result7-screenshot]: images/screencapture-localhost-sistem-data-karyawan-Pegawai-2022-05-30-11_16_04.png
[result8-screenshot]: images/screencapture-localhost-sistem-data-karyawan-Pegawai-2022-05-30-11_20_17.png
[result9-screenshot]: images/screencapture-localhost-sistem-data-karyawan-Send-email-2022-05-30-11_26_47.png
