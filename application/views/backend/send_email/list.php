<!-- Bread crumb and right sidebar toggle -->
<!-- Bread crumb and right sidebar toggle -->
<div class="row page-titles page-data" data-url="<?= $url ?>" data-module="<?= $module ?>">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?= $title ?></h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><a href="<?= site_url($url) ?>"><?= $title ?></a></li>
            </ol>
        </div>
    </div>
</div>
<!-- End Bread crumb and right sidebar toggle -->

<!-- Form -->
<form id="form" autocomplete="off">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-primary">
                    <h4 class="m-b-0 text-white">E-Mail Setting</h4>
                </div>
                <div class="card-body">
                    <input type="hidden" name="crud">
                    <input type="hidden" name="ID">
                    <input type="hidden" name="page_url">
                    <input type="hidden" name="page_module">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">protocol</label>
                                <input type="text" id="protocol" name="protocol" class="form-control">
                                <small class="form-control-feedback"></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">smtp_host</label>
                                <input type="text" id="smtp_host" name="smtp_host" class="form-control">
                                <small class="form-control-feedback"></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">smtp_port</label>
                                <input type="text" id="smtp_port" name="smtp_port" class="form-control">
                                <small class="form-control-feedback"></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">smtp_user</label>
                                <input type="text" id="smtp_user" name="smtp_user" class="form-control">
                                <small class="form-control-feedback"></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">smtp_pass</label>
                                <div class="input-group">
                                    <input type="password" id="smtp_pass" name="smtp_pass" class="form-control">
                                    <div class="input-group-append pointer show_password">
                                        <span class="input-group-text"><i class="btn-icon fa fa-eye"></i></span>
                                    </div>
                                </div>
                                <small class="form-control-feedback"></small>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-primary btn-save-mail" onclick="save_mail()"><i class="fa fa-save"></i>Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-primary">
                    <h4 class="m-b-0 text-white">Compose E-Mail</h4>
                </div>
                <div class="card-body">
                    <input type="hidden" name="crud">
                    <input type="hidden" name="ID">
                    <input type="hidden" name="page_url">
                    <input type="hidden" name="page_module">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">To</label>
                                <input type="text" id="to" name="to" class="form-control">
                                <small class="form-control-feedback"></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Subject</label>
                                <input type="text" id="subject" name="subject" class="form-control">
                                <small class="form-control-feedback"></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="inline-editor">
                                        <textarea class="form-control summernote_isi" name="isi" id="isi"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-primary btn-save" onclick="save()"><i class="fa fa-save"></i>Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- End Form -->

<!-- Start Page Content -->
<div class="row list-view">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-primary">
                <h4 class="m-b-0 text-white">List Data</h4>
            </div>
            <div class="card-body">
                <table id="table-list" class="tablesaw table-bordered table-hover table-cursor table table-responsive-lg">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Subjek</th>
                            <th>Kepada</th>
                            <th>Tanggal Kirim</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('assets/node_modules/datatables/jquery.dataTables.min.js') ?>"></script>

<!-- Plugin -->
<link href="<?= base_url('assets/node_modules/summernote/dist/summernote.css') ?>" rel="stylesheet"/>
<script src="<?= base_url('assets/node_modules/summernote/dist/summernote.min.js') ?>" type="text/javascript"></script>


<script src="<?= base_url('asset/js/custom/backend/send_email.js') ?>"></script>
