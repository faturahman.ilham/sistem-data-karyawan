<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model("M_jabatan","jabatan");
    }

    public function index(){
		$url 				= $this->uri->segment(1);
		$module 			= "jabatan";

        $btn_add            = $this->main->button('add_new');
        $btn_import         = 'import';

		$data["title"] 		= "Master Data Jabatan";
		$data['url']		= $url;
		$data['module']		= $module;
		$data['content'] 	= 'backend/jabatan/list';
		$data['form']		= 'backend/jabatan/form';
		$data['filter']		= 'backend/jabatan/filter';
		$data['import']		= 'backend/jabatan/import';
		$data['btn_add']	= $btn_add;
		$data['btn_import']	= $btn_import;
		$this->load->view('backend/index',$data);
	}

	public function list(){
		$data 	= array();
		$list 	= $this->jabatan->get_datatables();
		$i 		= $this->input->post('start');

		$url 		= $this->input->post('page_url');
		$module 	= $this->input->post('page_module');
		$MenuID 	= 1;
		$read_menu 	= $this->api->read_menu($MenuID);

		foreach ($list as $a):
			$no = $i++;
			$no += 1;
			$txt_status = $this->main->label_active($a->status);
			$tg_data = ' data-id="'.$a->id_jabatan.'" ';
			$tg_data .= ' data-status="'.$a->status.'" ';

			$btn_array = array();
			$btn_status = false;
			if($a->status == 0 and $read_menu->update>0):
				$btn_status = true;
				array_push($btn_array, 'edit');
			endif;
			if($a->status == 0 and $read_menu->delete>0):
				$btn_status = true;
				array_push($btn_array, 'delete');
			endif;

			if($btn_status):
				$btn = $this->main->button('action_list',$a->id_jabatan,$btn_array);
			else:
				$btn = '';
			endif;
			$temp = '<span class="dt-id" '.$tg_data.'>'.$no.'</span>';
			
			$row = array();
			$row[] 	= $btn.$temp;
			$row[] 	= $a->nama_jabatan;
			$row[] 	= $this->main->label_background($txt_status);
			$data[] = $row;
		endforeach;

		$response = array(
			"draw" 				=> $this->input->post('draw'),
			"recordsTotal" 		=> $this->jabatan->count_all(),
			"recordsFiltered" 	=> $this->jabatan->count_filtered(),
			"data" 				=> $data,
		);

		$this->main->echoJson($response);
	}

	public function save(){
		$url 				= $this->input->post('page_url');
		$module 			= $this->input->post('page_module');
		$MenuID 			= 1;
		$read_menu 			= $this->api->read_menu($MenuID);

		$Crud 		= $this->input->post("crud");
		if($Crud == "insert"): $p3 = $read_menu->insert;
		elseif($Crud == "update"): $p3 = $read_menu->update;
		else: $p3 = 0;
		endif;

		$this->main->check_session('out',$Crud, $p3);
		$this->validation->jabatan();

		$ID 		= $this->input->post("ID");
		$Nama 		= $this->input->post("Nama");

		$data = array(
			"nama_jabatan"  	=> $Nama,
		);
		if($Crud == "insert"):
			$message = "Insert Success";
			$this->main->general_save("mt_data_jabatan", $data);
		elseif($Crud == "update"):
			$message = "Update Success";
			$this->main->general_update("mt_data_jabatan", $data, array("id_jabatan" => $ID));
		endif;

		$response = array(
			"status"	=> true,
			"message"	=> $message,
		);
		$this->main->echoJson($response);
	}

	public function edit(){
		$url 				= $this->input->post('page_url');
		$module 			= $this->input->post('page_module');
		$MenuID 			= 1;
		$read_menu 			= $this->api->read_menu($MenuID);
		$this->main->check_session('out', 'update', $read_menu->update);
		$ID 		= $this->input->post('ID');
		$list 		= $this->jabatan->get_by_id($ID);

		$response = array(
			"status"	=> true,
			"data"		=> $list,
			"message"	=> "Edit Success",
		);

		$this->main->echoJson($response);
	}

	public function delete(){
		$url 				= $this->input->post('page_url');
		$module 			= $this->input->post('page_module');
		$MenuID 			= 1;
		$read_menu 			= $this->api->read_menu($MenuID);

		$this->main->check_session('out', 'delete', $read_menu->delete);
		$ID = $this->input->post('ID');

		$this->db->where("id_jabatan", $ID);
		$this->db->delete("mt_data_jabatan");

		$response = array(
			"status"	=> true,
			"message"	=> "Delete Success",
		);

		$this->main->echoJson($response);
	}

}