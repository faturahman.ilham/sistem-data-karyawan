<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->lang->load('bahasa', "english");
    }

    public function index()
	{
        $url    = "dashboard";
        $module = "dashboard";

        $data["title"]      = "Dashboard";
        $data['url']        = $url;
        $data['module']     = $module;
        $data['content']    = 'backend/page/dashboard';
        $this->load->view('backend/index',$data);
	}
	
	public function error_404(){
		$data["title"] 		= "404";
		$data['content'] 	= 'backend/page/error_404';
		$this->load->view('backend/index',$data);
	}

	public function error_403(){
		$data["title"] 		= "403";
		$data['content'] 	= 'backend/page/error_403';
		$this->load->view('backend/index',$data);
	}
}