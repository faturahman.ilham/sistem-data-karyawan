<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User Profile-->
        <div class="user-profile">
            <div class="user-pro-body">
                <div><img src="<?= base_url("assets/images/users/1.jpg") ?>" alt="user-img" class="img-circle"></div>
            </div>
        </div>
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);" aria-expanded="true"><i class="icon-speedometer"></i><span class="hide-menu">Master</span></a>
                    <ul aria-expanded="false" class="collapse in">
                        <li><a href="<?= base_url("Jabatan") ?> ">Data Jabatan Pegawai</a></li>
                        <li><a href="<?= base_url("Pegawai") ?> ">Data Pegawai</a></li>
                        <li><a href="<?= base_url("Kontrak") ?> ">Data Kontrak</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);" aria-expanded="true"><i class="mdi mdi-email"></i><span class="hide-menu">E-Mail</span></a>
                    <ul aria-expanded="false" class="collapse in">
                        <li><a href="<?= base_url("Send_email") ?> ">Send E-Mail</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>