<?php
	$HakAksesType   = $this->session->HakAksesType;
?>
<div class="col-12 p-0">
    <form id="form-filter">
    	<div class="row">
	        <div class="col-12">
				<div class="pull-right">
					<div class="form-group">
						<label class="control-label"><?= $this->lang->line('lb_search') ?></label>
						<div class="input-group">
							<div class="input-group-append">
								<span class="input-group-text"><i class="btn-icon fa fa-search"></i></span>
							</div>
							<input type="text" name="f-Search" class="form-control f-Search">
						</div>
						<small class="form-control-feedback"></small>
					</div>
				</div>
	        </div>
	        <div class="col-12">
	        	<div class="pull-right">
	        		<?= $this->main->button('filter',array('search')) ?>
	        	</div>
	        </div>
    	</div>
    </form>
</div>